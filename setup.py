"""
Configures this package for installation. See
https://docs.djangoproject.com/en/1.5/intro/reusable-apps/
for details.
"""

import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.md')).read()
LICENSE = open(os.path.join(os.path.dirname(__file__), 'LICENSE')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='cloudstorage',
    version='0.0.0-unknown',
    packages=['cloudstorage'],
    include_package_data=True,
    license=LICENSE,
    description='API for using Google Cloud Storage aka Blobstore',
    long_description=README,
    install_requires=[],
)

# vim: set ai et sw=4 syntax=python :
